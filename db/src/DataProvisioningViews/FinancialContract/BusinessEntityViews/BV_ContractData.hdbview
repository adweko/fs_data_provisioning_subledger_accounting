view "sap.fsdm.content.DataProvisioningForSubLedger::BV_ContractData"(in i_tsfrom timestamp , in i_tsto timestamp  )
as 
select 

	fc."IDSystem",
	fc."FinancialContractID",
	fc."BusinessValidFrom", 
	fc."BusinessValidTo",
	fc."SystemValidFrom",
	fc."SystemValidTo",
	fc."C_ObjectHash", 
	fc."LifecycleStatus",
	fc."FinancialContractCategory",
	fc."FinancialContractType",
	fc."OTCDerivativeContractCategory",
	fc."SourceSystemID",
	fc."OriginalSigningDate",
	fc."DepositStartDate",
	fc."OpeningDate",
	fc."CurrentDepositEndDate",
	fc."ExpectedMaturityDate",
	fc."ClosingDate",
	fc."EffectiveDate",
	fc."SettlementDate",
	fc."FirstSettlementDate",
	fc."SecondSettlementDate",
	fc."TerminationDate",
	fc."MaturityDate",
	fc."FacilityStartDate",
	fc."FacilityEndDate",
	fc."ExpiryDate",
	fc."IssueDate",
	
	fc."ApprovedNominalAmount",
	fc."PurposeType",
	fc."AccountCurrency",
	fc."NominalAmountCurrency",
	fc."SettlementCurrency",
	fc."AccountCategory",
	fc."SwapCategory",
	fc."InterestRateSwapCategory",
	fc."SimpleCollateralCategory",
	
	intr."FirstInterestPeriodStartDate",
	intr."LastInterestPeriodEndDate",
	intr."DayCountConvention",
	intr."BusinessDayConvention",
	intr."InterestBusinessCalendar",
	
	ouca."ASSOC_OrgUnit.OrganizationalUnitID",

	cc."CompanyCode",
	
	pca."_ProductCatalogItem.ProductCatalogItem" as "ProductCatalogItem"
	
from 
	"sap.fsdm.content.DataProvisioningForSubLedger::CV_FinancialContract"(:i_tsfrom, :i_tsto) as fc
		left outer join
	"sap.fsdm.content.DataProvisioningForSubLedger::CV_Interest"(:i_tsfrom, :i_tsto) as intr
	on fc."C_ObjectHash" = intr."C_ObjectHash" and fc."IDSystem" = intr."IDSystem" and fc."FinancialContractID" = intr."FinancialContractID"
	and fc."BusinessValidFrom" = intr."BusinessValidFrom"
		left outer join
	"sap.fsdm.content.DataProvisioningForSubLedger::CV_OrganizationalUnitContractAssignment"(:i_tsfrom, :i_tsto) as ouca
	on fc."C_ObjectHash" = ouca."C_ObjectHash" and fc."IDSystem" = ouca."IDSystem" and fc."FinancialContractID" = ouca."FinancialContractID"
	and fc."BusinessValidFrom" = ouca."BusinessValidFrom"	
		left outer join
	"sap.fsdm.content.DataProvisioningForSubLedger::CV_CompanyCode"(:i_tsfrom, :i_tsto) as cc
	on fc."C_ObjectHash" = cc."C_ObjectHash" and fc."IDSystem" = cc."IDSystem" and fc."FinancialContractID" = cc."FinancialContractID"
	and fc."BusinessValidFrom" = cc."BusinessValidFrom"
	    left outer join
	"sap.fsdm.content.DataProvisioningForSubLedger::CV_ProductCatalogAssignment"(:i_tsfrom, :i_tsto) as pca
	on fc."C_ObjectHash" = pca."C_ObjectHash" and fc."IDSystem" = pca."IDSystem" and fc."FinancialContractID" = pca."FinancialContractID"
	and fc."BusinessValidFrom" = pca."BusinessValidFrom"

where 

--exclude cases that are handled in other BV views
( fc."AccountCategory"				is null	or not fc."AccountCategory" 			= 'SecuritiesAccount' )			and	--handled in BV_SecuritiesAccount.hdbview 
( fc."SwapCategory" 				is null	or not fc."SwapCategory"				= 'CrossCurrencySwap' ) 		and	--handeld in BV_ContractDataStructured.hdbview
( fc."InterestRateSwapCategory" 	is null	or not fc."InterestRateSwapCategory"	= 'ComplexInterestRateSwap' )
