view "sap.fsdm.content.DataProvisioningForSubLedger::CV_ContractChangedVersionBusinessDates"(in i_tsfrom timestamp , in i_tsto timestamp  )

as 

select

	jointable."IDSystem",
	jointable."FinancialContractID",
	jointable."BusinessValidFrom",
	"C_ObjectHash"

from 
(
	-- Union of all newly created versions 
	
	select "IDSystem", "FinancialContractID", "BusinessValidFrom", "BusinessValidTo", "SourceSystemID", "C_ObjectHash"
		from
			(
				select "IDSystem", "FinancialContractID", "BusinessValidFrom", "BusinessValidTo", "SourceSystemID", "C_ObjectHash" from "sap.fsdm.content.DataProvisioningForSubLedger::EV_FinancialContract"
					where "SystemValidFrom" > :i_tsfrom and "SystemValidFrom" <= :i_tsto
			union
				
				select "IDSystem", "FinancialContractID", "BusinessValidFrom", "BusinessValidTo", "SourceSystemID", "C_ObjectHash" from "sap.fsdm.content.DataProvisioningForSubLedger::EV_Interest"
					where "SystemValidFrom" > :i_tsfrom and "SystemValidFrom" <= :i_tsto
			union
						
				select "IDSystem", "FinancialContractID", "BusinessValidFrom", "BusinessValidTo", "SourceSystemID", "C_ObjectHash" from "sap.fsdm.content.DataProvisioningForSubLedger::EV_OrganizationalUnitContractAssignment"
					where "RoleOfOrganizationalUnit" = 'ManagingUnit' and "SystemValidFrom" > :i_tsfrom and "SystemValidFrom" <= :i_tsto
			union
						
				select bpca."ASSOC_FinancialContract.IDSystem" as "IDSystem", bpca."ASSOC_FinancialContract.FinancialContractID" as "FinancialContractID", cc."BusinessValidFrom",  cc."BusinessValidTo", cc."SourceSystemID", bpca."C_ObjectHash" 
					from "sap.fsdm.content.DataProvisioningForSubLedger::EV_CompanyCode" as cc
						join
						 "sap.fsdm.content.DataProvisioningForSubLedger::EV_BusinessPartnerContractAssignment" as bpca
					on cc."ASSOC_Company.BusinessPartnerID" = bpca."ASSOC_PartnerInParticipation.BusinessPartnerID"
					where cc."SystemValidFrom" > :i_tsfrom and cc."SystemValidFrom" <= :i_tsto
				
			union
						
				select "ASSOC_FinancialContract.IDSystem" as "IDSystem", "ASSOC_FinancialContract.FinancialContractID" as "FinancialContractID", "BusinessValidFrom", "BusinessValidTo", "SourceSystemID", "C_ObjectHash" 
					from "sap.fsdm.content.DataProvisioningForSubLedger::EV_BusinessPartnerContractAssignment"
					where "SystemValidFrom" > :i_tsfrom and "SystemValidFrom" <= :i_tsto 
			union
						
				select "ASSOC_FinancialContract.IDSystem" as "IDSystem", "ASSOC_FinancialContract.FinancialContractID" as "FinancialContractID", "BusinessValidFrom", "BusinessValidTo", "SourceSystemID", "C_ObjectHash" 
					from "sap.fsdm.content.DataProvisioningForSubLedger::EV_AgreedLimit"
					where "SystemValidFrom" > :i_tsfrom and "SystemValidFrom" <= :i_tsto
					
			union
						
				select "_SwapForSchedule.IDSystem" as "IDSystem", "_SwapForSchedule.FinancialContractID" as "FinancialContractID", "BusinessValidFrom", "BusinessValidTo", "SourceSystemID", "C_ObjectHash" 
					from "sap.fsdm.content.DataProvisioningForSubLedger::EV_NotionalSchedule"
					where "SystemValidFrom" > :i_tsfrom and "SystemValidFrom" <= :i_tsto		
			
			union
						
				select "_SyndicationAgreement.IDSystem" as "IDSystem", "_SyndicationAgreement.FinancialContractID" as "FinancialContractID", "BusinessValidFrom", "BusinessValidTo", "SourceSystemID", "C_ObjectHash" 
					from "sap.fsdm.content.DataProvisioningForSubLedger::EV_TrancheInSyndication"
					where "SystemValidFrom" > :i_tsfrom and "SystemValidFrom" <= :i_tsto	
					
			union
			
				select syn."IDSystem", syn."FinancialContractID", con."BusinessValidFrom", con."BusinessValidTo", con."SourceSystemID", syn."C_ObjectHash" from
					"sap.fsdm.content.DataProvisioningForSubLedger::EV_FinancialContract" con inner join
					"sap.fsdm.content.DataProvisioningForSubLedger::EV_FinancialContract" syn on
					con."_TrancheInSyndication._SyndicationAgreement.FinancialContractID"	= syn."FinancialContractID" and 
					con."_TrancheInSyndication._SyndicationAgreement.IDSystem"          	= syn."IDSystem" and 
					con."C_ObjectHash"          											= syn."C_ObjectHash"
					where con."SystemValidFrom" > :i_tsfrom and con."SystemValidFrom" <= :i_tsto
								
			union
			
				select "IDSystem", "FinancialContractID", "InitDate" as "BusinessValidFrom", '99991231' as "BusinessValidTo", "SourceSystemID", "C_ObjectHash" from
					"sap.fsdm.content.DataProvisioningForSubLedger::EV_FinancialContractInit"
					where "Active" = 1
				
			)
			
) as jointable

		join
	"sap.fsdm.content.DataProvisioningForSubLedger::Initialization" as init
	on ( init."SourceSystemID" = jointable."SourceSystemID" or init."SourceSystemID" = Null or init."SourceSystemID" = '' )
	and ( init."InitDate" = jointable."BusinessValidFrom" or init."Active" = 0 )  /* note that a version on the InitDate is added above */
	and ( init."InitDate" < jointable."BusinessValidTo" )

group by 
"IDSystem",
"FinancialContractID",
"BusinessValidFrom",
"C_ObjectHash"